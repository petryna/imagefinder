package imagefinder.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import imagefinder.core.AvailableServicesMessage;
import imagefinder.core.ClientConnectionRequest;
import imagefinder.core.FinderRequest;
import imagefinder.core.Message;
import imagefinder.core.WorkerConnectionRequest;
import imagefinder.pcd.concurrency.BlockingQueue;
import imagefinder.pcd.networking.Connection;
import imagefinder.pcd.networking.SocketConnection;

public class Server extends Thread {

	public static final int PORT = 3080;

	private static final Server INSTANCE = new Server();

	public static Server getInstance() {
		return INSTANCE;
	}

	private ServerSocket server;
	private Map<Long, ConnectionToClient> clients;
	private long clientsCounter;
	private Map<String, List<ConnectionToWorker>> workers;
	private Map<String, BlockingQueue<FinderRequest>> services;

	public Server() {
		try {
			this.server = new ServerSocket(PORT);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		clientsCounter = 0;
		clients = new HashMap<Long, ConnectionToClient>();
		workers = new HashMap<String, List<ConnectionToWorker>>();
		services = new HashMap<String, BlockingQueue<FinderRequest>>();
	}

	@Override
	public void run() {
		while (!interrupted()) {
			newConnection();
		}
	}

	private void newConnection() {
		try {
			Socket socket = server.accept();
			new Thread() {
				@Override
				public void run() {
					try {
						doConnection(socket);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void doConnection(Socket socket) throws IOException {
		Connection connection = new SocketConnection(socket);
		Message request;
		try {
			request = (Message) connection.receiveObject();
			System.out.println(request);
			switch (request.getType()) {
			case ClientConnection:
				addClient(connection, (ClientConnectionRequest) request);
				break;
			case WorkerConnection:
				addWorker(connection, (WorkerConnectionRequest) request);
				break;
			default:
				connection.closeConnection();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	private void addClient(Connection connection, ClientConnectionRequest request) {
		long id = clientsCounter++;
		ConnectionToClient client = new ConnectionToClient(connection, id, request);
		client.start();
		addClient(client);
	}

	private void addWorker(Connection connection, WorkerConnectionRequest request) {
		ConnectionToWorker worker = new ConnectionToWorker(connection, request);
		addWorker(worker);
	}

	public void addClient(ConnectionToClient client) {
		synchronized (clients) {
			clients.put(client.getClientId(), client);
		}
		Message message = new AvailableServicesMessage(getAvailableServices(), client.getClientId());
		client.sendMessage(message);
	}

	public void removeClient(long clientId) {
		synchronized (clients) {
			clients.get(clientId).interrupt();
			clients.remove(clientId);
		}
	}

	public int getNumberOfClients() {
		synchronized (clients) {
			return clients.size();
		}
	}

	public ConnectionToClient getClient(long id) {
		synchronized (clients) {
			return clients.get(id);
		}
	}

	public void addWorker(ConnectionToWorker worker) {

		synchronized (workers) {
			if (!workers.containsKey(worker.getServiceType())) {
				workers.put(worker.getServiceType(), new LinkedList<ConnectionToWorker>());
			}
			workers.get(worker.getServiceType()).add(worker);
		}

		synchronized (services) {
			if (!services.containsKey(worker.getServiceType())) {
				services.put(worker.getServiceType(), new BlockingQueue<FinderRequest>());
			}
		}

		worker.start();

		availableServices();

	}

	public void removeWorker(ConnectionToWorker worker) {
		boolean removeService = false;
		synchronized (workers) {
			workers.get(worker.getServiceType()).remove(worker);
			if (workers.get(worker.getServiceType()).isEmpty()) {
				worker.interrupt();
				workers.remove(worker.getServiceType());
				removeService = true;
			}
		}

		if (removeService) {
			synchronized (services) {
				worker.interrupt();
				services.remove(worker.getServiceType());
			}
		}
		availableServices();

	}

	public int getNumberOfWorkers() {
		synchronized (workers) {
			return workers.size();
		}
	}

	public void close() {
		try {
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addRequest(FinderRequest request) {
		try {
			BlockingQueue<FinderRequest> queue = services.get(request.getServiceType());
			if (queue != null) {
				queue.offer(request);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public FinderRequest getRequest(ConnectionToWorker worker) {
		try {
			BlockingQueue<FinderRequest> queue = services.get(worker.getServiceType());
			if (queue != null) {
				return queue.poll();
			}
			return null;
		} catch (InterruptedException e) {
			return null;
		}
	}

	public void putMessage(Message message) {
		ConnectionToClient client = getClient(message.getConnectionId());
		if (client != null) {
			client.sendMessage(message);
		}
	}

	private void availableServices() {
		Map<String, Integer> availableServices = getAvailableServices();
		synchronized (clients) {
			for (Entry<Long, ConnectionToClient> e : clients.entrySet()) {
				Message message = new AvailableServicesMessage(availableServices, e.getKey());
				e.getValue().sendMessage(message);
			}
		}
	}

	public Map<String, Integer> getAvailableServices() {
		Map<String, Integer> result = new HashMap<String, Integer>();
		synchronized (workers) {
			for (Entry<String, List<ConnectionToWorker>> e : workers.entrySet()) {
				result.put(e.getKey(), e.getValue().size());
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Server.getInstance().start();
	}

}
