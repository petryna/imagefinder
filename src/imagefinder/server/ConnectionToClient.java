package imagefinder.server;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import imagefinder.core.ClientConnectionRequest;
import imagefinder.core.FinderRequest;
import imagefinder.core.Message;
import imagefinder.pcd.networking.Connection;

public class ConnectionToClient extends Thread {

	private Connection connection;
	private long id;
	private PropertyChangeListener connectionListener;
	boolean active = true;

	public ConnectionToClient(Connection connection, long id, ClientConnectionRequest request) {
		connectionListener = new ListenerManager();
		connection.addConnectionListener(connectionListener);
		this.connection = connection;
		this.id = id;
	}

	@Override
	public void run() {
		Message request;
		try {
			while (!interrupted() && active) {
				request = (Message) connection.receiveObject();
				if (request != null && request.getConnectionId() == id) {
					System.out.println(request);
				} else {
					continue;
				}
				switch (request.getType()) {
				case FinderRequest:
					finderRequest((FinderRequest) request);
					break;
				default:
					break;
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			System.out.println("Client connection closed");
		}
	}

	private void closeConnection() {
		try {
			Server.getInstance().removeClient(id);
			System.out.println("Removed Client");
			connection.closeConnection();
			active = false;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void finderRequest(FinderRequest request) {
		Server.getInstance().addRequest(request);
	}

	public void sendMessage(Message message) {
		System.out.println(message);
		connection.sendObject(message);
	}

	public Long getClientId() {
		return id;
	}

	public class ListenerManager implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent e) {
			closeConnection();
		}
	}

}
