package imagefinder.server;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import imagefinder.core.FinderRequest;
import imagefinder.core.Message;
import imagefinder.core.WorkerConnectionRequest;
import imagefinder.pcd.networking.Connection;

public class ConnectionToWorker extends Thread {

	private PropertyChangeListener connectionListener;
	private Connection connection;
	private String serviceType;
	private boolean active = true;

	public ConnectionToWorker(Connection connection, WorkerConnectionRequest request) {
		this.connectionListener = new ListenerManager();
		connection.addConnectionListener(this.connectionListener);
		this.connection = connection;
		this.serviceType = request.getServiceType();
	}

	@Override
	public void run() {
		try {
			while (!interrupted() && active) {
				FinderRequest request = Server.getInstance().getRequest(this);
				if (request != null) {
					connection.sendObject(request);
					Message response;

					response = (Message) connection.receiveObject();
					if (response == null || response.getType() != Message.MessageType.FinderResult) {
						Server.getInstance().addRequest(request);
					} else {
						Server.getInstance().putMessage(response);
					}
				}
			}
		} catch (InterruptedException e) {
			e.getStackTrace();
		} finally {
			System.out.println("Worker connection closed");
		}
	}

	private void closeConnection() {
		try {
			Server.getInstance().removeWorker(this);
			System.out.println("Worker Removed");
			connection.closeConnection();
			active = false;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String getServiceType() {
		return this.serviceType;
	}

	public class ListenerManager implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent e) {
			closeConnection();
		}
	}

}
