package imagefinder.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import imagefinder.core.FinderResult;
import imagefinder.core.FinderRequest;
import imagefinder.core.SimpleFinder;
import imagefinder.pcd.networking.SocketConnection;
import imagefinder.server.Server;

@SuppressWarnings("unused")

public class ImageFinderApp {

	private JFrame frame;
	private JFileChooser jfc;

	private JPanel centerPanel;
	// centerPanelComponents:
	private JLabel imageLabel;
	private JScrollPane imageScrollPane;
	private JList<ResultModel> imageNameList;
	private JList<ServiceModel> searchServices;
	private DefaultListModel<ServiceModel> serviceListModel;

	private DefaultListModel<ResultModel> folderListModel;

	private JPanel lowerPanel;
	// lowerPanelComponents:
	private JTextField selectedFolderPath;
	private JTextField selectedLogoPath;
	private JButton folderSelect;
	private JButton imageLogoSelect;
	private JButton searchAction;

	// Client
	private Client client;
	// sentinels (types):
	private static SentinelButtons searchSentinel;
	private boolean isChanging = false;

	public ImageFinderApp(Client client) {
		this.client = client;
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				client.endSession();
			}
		});
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				buildFrame();
			}
		});
	}

	private void buildFrame() {
		frame = new JFrame("ImageFinderApp");
		frame.setLayout(new BorderLayout());
		buildCenterPanel();
		buildLowerPanel();
		frame.setSize(1200, 700);
		frame.setLocation(100, 100);
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	private void buildCenterPanel() {
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		// elements of centerPanel
		imageLabel = new JLabel();
		imageScrollPane = new JScrollPane(imageLabel);
		// serviceListModel
		serviceListModel = new DefaultListModel<ServiceModel>();
		folderListModel = new DefaultListModel<ResultModel>();
		searchServices = new JList<ServiceModel>(serviceListModel);
		imageNameList = new JList<ResultModel>(getFolderListModel());
		searchServices.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		serviceListModel.addElement(new ServiceModel("Simple Search", 1));
		serviceListModel.addElement(new ServiceModel("90 Search", 1));
		serviceListModel.addElement(new ServiceModel("180 Search", 1));
		// addElements
		centerPanel.add(searchServices, BorderLayout.WEST);
		centerPanel.add(imageScrollPane, BorderLayout.CENTER);
		centerPanel.add(imageNameList, BorderLayout.EAST);
		// ActionListeners
		imageNameList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						if (!arg0.getValueIsAdjusting()) {
							if (imageNameList.getSelectedIndex() > -1) {
								displayImage();
							}
						}
					}
				});
			}
		});
		// addToFrame
		frame.add(centerPanel, BorderLayout.CENTER);
		// dimensions
		searchServices.setPreferredSize(new Dimension(250, 750));
		imageNameList.setPreferredSize(new Dimension(250, 750));
	}

	private void buildLowerPanel() {
		lowerPanel = new JPanel();
		lowerPanel.setLayout(new BorderLayout());
		JPanel buttonsPanel = new JPanel(new GridLayout(2, 1));
		JPanel textPanel = new JPanel(new GridLayout(2, 1));
		// elements of lowerPanel
		selectedFolderPath = new JTextField();
		selectedFolderPath.setEditable(false);
		selectedLogoPath = new JTextField();
		selectedLogoPath.setEditable(false);
		folderSelect = new JButton("Folder");
		imageLogoSelect = new JButton("Sub-Image");
		searchAction = new JButton("Search");
		// addElements
		textPanel.add(selectedFolderPath);
		textPanel.add(selectedLogoPath);
		buttonsPanel.add(folderSelect);
		buttonsPanel.add(imageLogoSelect);
		lowerPanel.add(textPanel, BorderLayout.CENTER);
		lowerPanel.add(buttonsPanel, BorderLayout.EAST);
		lowerPanel.add(searchAction, BorderLayout.SOUTH);
		// ActionListeners
		searchSentinel = new SentinelButtons();
		folderSelect.addActionListener(searchSentinel);
		imageLogoSelect.addActionListener(searchSentinel);
		searchAction.addActionListener(searchSentinel);
		// addToFrame
		frame.add(lowerPanel, BorderLayout.SOUTH);
		// dimensions
	}

	public class SentinelButtons implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String s = e.getActionCommand();

			if (s.equals("Folder")) {
				jfc = new JFileChooser(".");
				jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int folderReturnValue = jfc.showOpenDialog(frame);
				if (folderReturnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
					selectedFolderPath.setText(selectedFile.getAbsolutePath());
				}
			}
			if (s.equals("Sub-Image")) {
				jfc = new JFileChooser(".");
				jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int subReturnValue = jfc.showOpenDialog(frame);
				if (subReturnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
					selectedLogoPath.setText(selectedFile.getAbsolutePath());
				}
			}
			if (s.equals("Search")) {
				searchAction.setEnabled(false);
				// avoids display of results from previous search actions
				isChanging = true;
				getFolderListModel().clear();
				isChanging = false;
				imageLabel.setIcon(null);
				if (!selectedFolderPath.getText().isEmpty() && !selectedLogoPath.getText().isEmpty()
						&& !searchServices.isSelectionEmpty()) {
					Search();
				} else {
					JOptionPane.showMessageDialog(null, "Selected paths and/or type of service is not selected.");
					searchAction.setEnabled(true);
				}
			}
		}
	}

	/**
	 * builds a requestList using readDirectory(), then applies the result to build
	 * a answerList applying searchRequest(), and finally updates the result on the
	 * GUI
	 */
	private void Search() {
		new Thread() {
			@Override
			public void run() {
				System.out.println("Reading Diretory and preparing requests...");
				List<FinderRequest> requestList = readDirectory(selectedFolderPath.getText(),
						selectedLogoPath.getText());
				System.out.println("Fetching answerList...");
				List<FinderResult> answerList = client.searchRequest(requestList, requestList.get(0).getReference());
				System.out.println("merging results...");
				readAnswer(MergeMatches(answerList));
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						searchAction.setEnabled(true);
					}
				});
			}
		}.start();

	}

	private List<FinderResult> MergeMatches(List<FinderResult> answerList) {
		List<FinderResult> mergedAnswerList = new ArrayList<FinderResult>();
		Collections.sort(answerList);
		int count = 0;
		for (int i = 0; i != answerList.size(); i++) {
			if (i != answerList.size() - 1
					&& answerList.get(i).getImageId().equals(answerList.get(i + 1).getImageId())) {
				count++;
			} else {
				// startOfSubList = i - count
				// endOfSubList = i
				// if(i - count == i), subList(i-count,i) will return a empty list.
				if ((i - count) != i) {
					mergedAnswerList.add(mergeFinderResultList(answerList.subList(i - count, i + 1)));
				} else {
					mergedAnswerList.add(answerList.get(i));
				}
				count = 0;
			}
		}
		return mergedAnswerList;
	}

	private FinderResult mergeFinderResultList(List<FinderResult> subAnswerList) {
		List<Rectangle> mergedMatches = new LinkedList<Rectangle>();
		for (FinderResult fr : subAnswerList) {
			mergedMatches.addAll(fr.getMatches());
		}
		FinderResult mergeResult = new FinderResult(mergedMatches,
				new FinderRequest(subAnswerList.get(0).getImageId(), subAnswerList.get(0).getServiceType()));
		return mergeResult;
	}

	/**
	 * Reads a list of FinderAnswer, sorts it by toString() and applies it to the
	 * GUI folderListModel element, clearing previous results.
	 * 
	 * @param answerList
	 */
	private void readAnswer(List<FinderResult> answerList) {
		System.out.println("Reading answers...");
		Collections.sort(answerList);
		isChanging = true;
		getFolderListModel().clear();
		for (int i = 0; i != answerList.size(); i++) {
			getFolderListModel().addElement(new ResultModel(answerList.get(i)));
		}
		isChanging = false;
	}

	/**
	 * Displays the image from the selectedFolderPath on the JLabel and uses the
	 * imageNameList FinderAnswer to draw a red rectangle on each logo match before
	 * displaying.
	 */

	private void displayImage() {
		FinderResult selectedItem = this.imageNameList.getSelectedValue().getResult();
		if (selectedItem != null) {
			String imagePath = selectedFolderPath.getText() + File.separator + selectedItem.getImageId();
			new Thread() {
				@Override
				public void run() {
					try {
						BufferedImage image = ImageIO.read(new File(imagePath));
						Graphics2D g2d = image.createGraphics();
						g2d.setColor(Color.RED);
						for (int i = 0; i != selectedItem.getMatches().size(); i++) {
							g2d.drawRect(selectedItem.getMatches().get(i).x, selectedItem.getMatches().get(i).y,
									selectedItem.getMatches().get(i).width, selectedItem.getMatches().get(i).height);
						}
						g2d.dispose();
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								imageLabel.setIcon(new ImageIcon(image));
							}
						});
					} catch (IOException e) {
						System.err.println("Unable to load " + imagePath + " to display.");
						e.printStackTrace();
					}
				}
			}.start();
		} else
			return;
	}

	/**
	 * Reads the directory in selectedFolderPath, updates the pane on the right, and
	 * returns a List of FinderRequest.
	 * 
	 * @return List<FinderRequest> requestList.
	 */
	private List<FinderRequest> readDirectory(String folderPath, String logoPath) {
		List<FinderRequest> requestList = new LinkedList<FinderRequest>();
		List<ServiceModel> servicesList = searchServices.getSelectedValuesList();
		BufferedImage logoImage = null;
		String path = folderPath;
		File[] listOfFiles = new File(path).listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File path, String name) {
				return name.endsWith(".png") || name.endsWith(".PNG");
			}
		});
		try {
			logoImage = ImageIO.read(new File(logoPath));
		} catch (IOException e1) {
			System.err.println("Unable to load the logo image from the given path.");
			JOptionPane.showMessageDialog(null, "Unable to load logo from the given path.");
			e1.printStackTrace();
		}
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				String file = listOfFiles[i].getName();
				try {
					BufferedImage image = ImageIO.read(new File(path + File.separator + file));
					for (int y = 0; y != servicesList.size(); y++) {
						requestList.add(new FinderRequest(logoImage, image, file, servicesList.get(y).getServiceType(),
								client.getConnectionId(), client.getRequestId()));
						client.getConnection().sendObject(new FinderRequest(logoImage, image, file,
								servicesList.get(y).getServiceType(), client.getConnectionId(), client.getRequestId()));
					}
				} catch (IOException e) {
					System.err.println("Unable to load images from the given path.");
					JOptionPane.showMessageDialog(null, "Unable to load images from the given path.");
					e.printStackTrace();
				}
			}
		}
		return requestList;
	}

	public JFrame getFrame() {
		return this.frame;
	}

	public DefaultListModel<ServiceModel> getServiceListModel() {
		return serviceListModel;
	}

	public static void main(String[] args) {
		if (args.length > 0) {
			try {
				NetworkClient client = new NetworkClient(new SocketConnection(args[0], Server.PORT));
				ImageFinderApp window = new ImageFinderApp(client);
				client.setGUI(window);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Connection Error");
				System.exit(1);
			}
		}

		if (args.length == 0) { // local main
			System.out.println("Running imagefinder locally...");
			try {
				LocalClient client = new LocalClient();
				ImageFinderApp window = new ImageFinderApp(client);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Connection Error");
				System.exit(1);
			}
		} else {

		}
	}

	protected DefaultListModel<ResultModel> getFolderListModel() {
		return folderListModel;
	}

}
