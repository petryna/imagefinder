package imagefinder.client;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;

import imagefinder.core.ClientConnectionRequest;
import imagefinder.core.FinderRequest;
import imagefinder.core.FinderResult;
import imagefinder.core.AvailableServicesMessage;
import imagefinder.pcd.concurrency.BlockingQueue;
import imagefinder.pcd.networking.Connection;

public class NetworkClient implements Client {

	/**
	 * Runs ImageFinder with the server distributed solution
	 */

	private ImageFinderApp GUI;
	private Connection connection;
	private Thread receiver;
	private BlockingQueue<FinderResult> results;
	private PropertyChangeListener connectionListener;
	private long connectionId;
	private long requestId = 0;

	public NetworkClient(Connection connection) {
		this.connection = connection;
		connectionListener = new ListenerManager();
		connection.addConnectionListener(connectionListener);
		results = new BlockingQueue<FinderResult>();
		receiver = new Receiver();
		receiver.start();
		connection.sendObject(new ClientConnectionRequest());
	}

	private class Receiver extends Thread {
		@Override
		public void run() {
			while (!interrupted()) {
				Object receivedResult;
				try {
					receivedResult = connection.receiveObject();
					if (receivedResult != null) {
						if (receivedResult instanceof AvailableServicesMessage) {
							JOptionPane.showMessageDialog(null, "Received answer from server!");
							connectionId = ((AvailableServicesMessage) receivedResult).getConnectionId();
							GUI.getServiceListModel().clear();
							if (!((AvailableServicesMessage) receivedResult).getAvailableServiceList().isEmpty()) {
								Map<String, Integer> services = new HashMap<String, Integer>();
								services = ((AvailableServicesMessage) receivedResult).getAvailableServiceList();
								for (Entry<String, Integer> e : services.entrySet()) {
									GUI.getServiceListModel().addElement(new ServiceModel(e.getKey(), e.getValue()));
								}
							} else {
								JOptionPane.showMessageDialog(GUI.getFrame(),
										"Server holds no services at the moment.");
							}
						}
						if (receivedResult instanceof FinderResult) {
							try {
								if (((FinderResult) receivedResult).getRequestId() == requestId) {
									GUI.getFolderListModel().addElement(new ResultModel((FinderResult) receivedResult));
									results.offer((FinderResult) receivedResult);
									synchronized (NetworkClient.this) {
										NetworkClient.super.notifyAll();
									}
								}
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				} catch (InterruptedException e1) {
					endSession();
				}

			}
		}
	}

	@Override
	public List<FinderResult> searchRequest(List<FinderRequest> requests, Image logo) {
		// for (FinderRequest fr : requests) {
		// connection.sendObject(fr);
		// }

		synchronized (this) {
			while (requests.size() > results.getSize()) {
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		try {
			List<FinderResult> resultTarget = results.pollQueue();
			requestId++;
			System.out.println(resultTarget.size());
			System.out.println(resultTarget.get(0).getServiceType());
			return resultTarget;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void endSession() {
		System.out.println("ending client session...");
		receiver.interrupt();
		if (!receiver.isInterrupted()) {
			try {
				receiver.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			connection.closeConnection();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public class ListenerManager implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent e) {
			System.out.println("Connection lost");
			JOptionPane.showMessageDialog(GUI.getFrame(), "Connection Lost");
		}
	}

	public long getConnectionId() {
		return connectionId;
	}

	public Connection getConnection() {
		return connection;
	}

	public long getRequestId() {
		return requestId;
	}

	public void setGUI(ImageFinderApp GUI) {
		this.GUI = GUI;
	}
}
