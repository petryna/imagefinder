package imagefinder.client;

import java.awt.Image;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import imagefinder.core.FinderResult;
import imagefinder.core.FinderRequest;
import imagefinder.core.AbstractFinder;
import imagefinder.core.SimpleFinder;
import imagefinder.pcd.networking.Connection;

/*
 * Runs ImageFinder locally.
 * 
 */
public class LocalClient implements Client {

	public LocalClient() {

	}

	@Override
	public List<FinderResult> searchRequest(List<FinderRequest> requests, Image logo) {

		List<FinderResult> results = new LinkedList<FinderResult>();
		Map<Thread, AbstractFinder> finders = new HashMap<Thread, AbstractFinder>();

		for (FinderRequest request : requests) {
			AbstractFinder imageFinder = new SimpleFinder(request);
			Thread t = new Thread(imageFinder);
			t.start();
			finders.put(t, imageFinder);
		}

		for (Entry<Thread, AbstractFinder> e : finders.entrySet()) {
			try {
				e.getKey().join();
				results.add(e.getValue().getResult());
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

		}

		return results;
	}

	@Override
	public long getRequestId() {
		return 0;
	}

	@Override
	public long getConnectionId() {
		return 0;
	}

	@Override
	public void endSession() {
		System.exit(1);

	}

	@Override
	public Connection getConnection() {
		return null;
	}

}