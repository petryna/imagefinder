package imagefinder.client;

public class ServiceModel {

	private String serviceType;

	private Integer numberAvailable;

	public ServiceModel(String serviceType, Integer numberAvailable) {
		this.serviceType = serviceType;
		this.numberAvailable = numberAvailable;
	}

	@Override
	public String toString() {
		return (this.serviceType + "- " + this.numberAvailable + " Available.");
	}

	public Integer getNumberAvailable() {
		return numberAvailable;
	}

	protected String getServiceType() {
		return serviceType;
	}

}
