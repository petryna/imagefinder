package imagefinder.client;

import java.awt.Image;
import java.util.List;

import imagefinder.core.FinderResult;
import imagefinder.pcd.networking.Connection;
import imagefinder.core.FinderRequest;

public interface Client {

	/**
	 * FinderRequest Returns a FinderAnswer list from the server, when given a
	 * FinderRequest list and a image as logo to search for.
	 * 
	 * 
	 * @param List<FinderRequest> requests - Represents a list of Requests, each
	 *                            containing a single image, and identifiers for the
	 *                            image and client respectively
	 * @param Image               logo - Represents a Image, given as logo to be
	 *                            searched for.
	 * @return List<FinderAnswer> - Represents a list of Answers, originating from
	 *         the server, each containing Coordinates and a identifier for the
	 *         image where the coordinates should be applied on, as well as a client
	 *         identifier.
	 */

	public List<FinderResult> searchRequest(List<FinderRequest> requests, Image logo);

	public long getRequestId();

	public long getConnectionId();

	public void endSession();

	public Connection getConnection();

}
