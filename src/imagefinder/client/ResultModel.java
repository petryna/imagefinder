package imagefinder.client;

import imagefinder.core.FinderResult;

public class ResultModel {

	private FinderResult result;

	public ResultModel(FinderResult result) {
		this.result = result;
	}

	protected FinderResult getResult() {
		return result;
	}

	@Override
	public String toString() {
		return (result.getImageId() + "- " + result.getMatches().size());
	}
}
