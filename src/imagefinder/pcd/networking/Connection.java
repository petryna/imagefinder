package imagefinder.pcd.networking;

import java.beans.PropertyChangeListener;

public abstract interface Connection {

	/**
	 * Interface for a network connection used by the server, clients and workers of
	 * the ImageFinder distributed solution.
	 */

	public void sendObject(Object a);

	public Object receiveObject() throws InterruptedException;

	public void closeConnection() throws InterruptedException;

	public void addConnectionListener(PropertyChangeListener listener);

	public void removeConnectionListener(PropertyChangeListener listener);

}
