package imagefinder.pcd.networking;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import imagefinder.pcd.concurrency.BlockingQueue;

public class SocketConnection implements Connection {
	private BlockingQueue<Object> inQueue;
	private BlockingQueue<Object> outQueue;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private Socket socket;
	private boolean server;
	private Thread sender;
	private Thread receiver;
	private PropertyChangeSupport propChangeSupport = new PropertyChangeSupport(this);
	private boolean active;

	public SocketConnection(String host, int port) throws UnknownHostException, IOException {
		server = false;
		socket = new Socket(InetAddress.getByName(host), port);
		initConnection();
		active = true;
	}

	public SocketConnection(Socket socket) throws IOException {
		server = true;
		this.socket = socket;
		initConnection();
		active = true;
	}

	@Override
	public void sendObject(Object a) {
		try {
			outQueue.offer(a);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object receiveObject() throws InterruptedException {
		Object retrievedObject = null;
		try {
			retrievedObject = inQueue.poll();
		} catch (InterruptedException e) {
			closeConnection();
		}
		return retrievedObject;
	}

	@Override
	public void closeConnection() throws InterruptedException {
		sender.interrupt();
		receiver.interrupt();
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initializes the input and output Object Streams and start the sender and
	 * receiver threads, effectively initializing the connection.
	 * 
	 * @throws IOException
	 */
	private void initConnection() throws IOException {
		inQueue = new BlockingQueue<Object>();
		outQueue = new BlockingQueue<Object>();
		// server or !server changes order of initializations.
		if (server) {

			input = new ObjectInputStream(socket.getInputStream());
			output = new ObjectOutputStream(socket.getOutputStream());

		} else {

			output = new ObjectOutputStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());

		}
		sender = new Sender();
		receiver = new Receiver();
		sender.start();
		receiver.start();
	}

	public void addConnectionListener(PropertyChangeListener listener) {
		propChangeSupport.addPropertyChangeListener(listener);
	}

	public void removeConnectionListener(PropertyChangeListener listener) {
		propChangeSupport.removePropertyChangeListener(listener);
	}

//threads inner classes
	private class Sender extends Thread {
		@Override
		public void run() {
			while (!interrupted()) {
				try {
					output.writeObject(outQueue.poll());
				} catch (IOException | InterruptedException e) {
					if (active) {
						active = false;
						try {
							closeConnection();
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
						propChangeSupport.firePropertyChange("sender", true, false);
					}
					break;
				}
			}
		}
	}

	private class Receiver extends Thread {
		@Override
		public void run() {
			while (!interrupted()) {
				try {
					inQueue.offer(input.readObject());
				} catch (InterruptedException | IOException e) {
					if (active) {
						active = false;
						try {
							closeConnection();
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
						propChangeSupport.firePropertyChange("receiver", true, false);
					}

					break;
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
