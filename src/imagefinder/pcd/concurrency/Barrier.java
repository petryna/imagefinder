package imagefinder.pcd.concurrency;

public class Barrier {

	private int barrierHold;
	private int barrierHolding = 0;
	private int barrierPassed;

	Barrier(int max) {
		this.barrierHold = max;
	}

	public synchronized void barrierWait() throws InterruptedException {
		barrierHolding++;
		while (barrierHolding < barrierHold) {
			wait();
		}
		if (barrierPassed == 0) {
			notifyAll();
		}
		barrierPassed++;
		if (barrierPassed == barrierHold) {
			barrierPassed = 0;
			barrierHolding = 0;
		}
	}
}
