package imagefinder.pcd.concurrency;

import java.util.LinkedList;
import java.util.List;

public class BlockingQueue<T> {
	private LinkedList<T> queue;
	private Integer maxElements = null;

	// constructors

	public BlockingQueue(int size) {
		this.queue = new LinkedList<T>();
		this.maxElements = size;
	}

	public BlockingQueue() {
		this.queue = new LinkedList<T>();
	}

	// methods

	public synchronized int getSize() {
		return queue.size();
	}

	public synchronized void offer(T a) throws InterruptedException {
		while (maxElements != null && queue.size() == maxElements) {
			wait();
		}
		queue.add(a);
		notifyAll();
	}

	public synchronized T poll() throws InterruptedException {
		while (queue.isEmpty()) {
			wait();
		}
		if (maxElements != null && queue.size() == maxElements) {
			T target = queue.removeFirst();
			notifyAll();
			return target;
		} else {
			return queue.removeFirst();
		}
	}

	public synchronized List<T> pollQueue() throws InterruptedException {
		while (maxElements != null && queue.size() != maxElements) {
			wait();
		}
		List<T> targetQueue = new LinkedList<T>();
		targetQueue.addAll(queue);
		queue.clear();
		notifyAll();
		return targetQueue;
	}

}