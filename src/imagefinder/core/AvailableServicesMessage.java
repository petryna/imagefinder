package imagefinder.core;

import java.util.Map;

public class AvailableServicesMessage extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4937906678286971666L;
	private Map<String, Integer> availableServiceList;

	public AvailableServicesMessage(Map<String, Integer> availableServices, long connectionId) {
		super(Message.MessageType.AvailableServices, connectionId);
		this.availableServiceList = availableServices;
	}

	public Map<String, Integer> getAvailableServiceList() {
		return availableServiceList;
	}

}
