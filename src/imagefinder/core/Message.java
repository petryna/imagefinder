package imagefinder.core;

import java.io.Serializable;

public abstract class Message implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6035349963166189370L;

	public static enum MessageType {
		WorkerConnection, ClientConnection, FinderRequest, FinderResult, AvailableServices
	}

	protected long connectionId;
	protected long requestId;

	private final MessageType type;

	public Message(MessageType type, long connectionId, long requestId) {
		this.type = type;
		this.connectionId = connectionId;
		this.requestId = requestId;
	}

	public Message(MessageType type, long connectionId) {
		this.type = type;
		this.connectionId = connectionId;
		this.requestId = -1;
	}

	public Message(MessageType type) {
		this.type = type;
		this.connectionId = -1;
		this.requestId = -1;
	}

	public MessageType getType() {
		return type;
	}

	public long getConnectionId() {
		return connectionId;
	}

	public long getRequestId() {
		return requestId;
	}

	@Override
	public String toString() {
		return type.toString() + " connectionId: " + connectionId + " requestId: " + requestId;
	}

}
