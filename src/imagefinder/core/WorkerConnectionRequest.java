package imagefinder.core;

public class WorkerConnectionRequest extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7029957229895599513L;

	private final String serviceType;

	public WorkerConnectionRequest(String serviceType, long connectionId, long requestId) {
		super(Message.MessageType.WorkerConnection, connectionId, requestId);
		this.serviceType = serviceType;
	}

	public WorkerConnectionRequest(String serviceType) {
		super(Message.MessageType.WorkerConnection);
		this.serviceType = serviceType;
	}

	public String getServiceType() {
		return serviceType;
	}

	@Override
	public String toString() {
		return super.toString() + " type: " + serviceType;
	}

}
