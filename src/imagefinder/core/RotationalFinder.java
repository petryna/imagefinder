package imagefinder.core;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

public class RotationalFinder extends SimpleFinder {

	public static enum Rotation {

		R90(90), R180(180), R270(270);

		private final int degrees;
		private final double radians;

		private Rotation(int degrees) {
			this.degrees = degrees;
			this.radians = Math.toRadians(degrees);
		}

		@Override
		public String toString() {
			return this.degrees + "�";
		}

	}

	private Rotation rotation;

	public RotationalFinder(FinderRequest request, Rotation rotation) {
		super(request);
		this.rotation = rotation;
	}

	@Override
	protected void search() {
		this.reference = rotateImage(this.reference, this.rotation.radians);
		super.search();
	}

	@Override
	public String toString() {
		return this.rotation + "Finder";
	}

	private static BufferedImage rotateImage(BufferedImage input, double radians) {

		AffineTransform transform = new AffineTransform();
		transform.rotate(radians, input.getWidth() / 2, input.getHeight() / 2);
		AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
		return op.filter(input, null);

	}

}
