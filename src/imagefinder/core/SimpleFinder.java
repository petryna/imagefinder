package imagefinder.core;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;

/**
 * Performs Simple Search on Images.
 *
 */
public class SimpleFinder extends AbstractFinder {

	protected BufferedImage image;
	protected BufferedImage reference;

	public SimpleFinder(FinderRequest request) {
		super(request);
		this.image = request.getImage();
		this.reference = request.getReference();
	}

	@Override
	protected void search() {

		Rectangle r = nextRectangle(null);

		while (r != null) {

			if (checkMatch(r)) {
				this.matches.add(r);
				r = new Rectangle(r.x + r.width, r.y, this.reference.getWidth(), this.reference.getHeight());
			}

			r = nextRectangle(r);

		}
	}

	protected Rectangle nextRectangle(Rectangle r) {

		if (r == null)
			return new Rectangle(0, 0, this.reference.getWidth(), this.reference.getHeight());

		int x = r.x + 1;
		int y = r.y;

		if (x >= this.image.getWidth() - this.reference.getHeight()) {
			x = 0;
			y = y + 1;
		}

		if (y >= this.image.getHeight() - this.reference.getHeight()) {
			return null;
		}

		return new Rectangle(x, y, this.reference.getWidth(), this.reference.getHeight());

	}

	protected boolean checkMatch(Rectangle r) {

		BufferedImage subImage = this.image.getSubimage(r.x, r.y, r.width, r.height);

		for (int x = 0; x < r.width; x++) {
			for (int y = 0; y < r.height; y++) {

				if (this.reference.getRGB(x, y) != subImage.getRGB(x, y)) {
					return false;
				}

			}
		}

		return true;
	}

	@Override
	public String toString() {
		return "SimpleFinder";
	}

}
