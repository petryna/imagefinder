package imagefinder.core;

public class ClientConnectionRequest extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2772935638982383540L;

	public ClientConnectionRequest() {
		super(Message.MessageType.ClientConnection);
	}

}
