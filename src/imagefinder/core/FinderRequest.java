package imagefinder.core;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class FinderRequest extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4358127128820681557L;
	private byte[] reference;
	private byte[] image;

	private String serviceType;

	/**
	 * Represents an identifier for the image object.
	 */
	private String imageId;

	public FinderRequest(String imageId, String serviceType) {
		super(Message.MessageType.FinderRequest);
		this.imageId = imageId;
		this.serviceType = serviceType;
	}

	public FinderRequest(BufferedImage reference, BufferedImage image, String imageId, String serviceType,
			Long connectionId, Long requestId) {
		super(Message.MessageType.FinderRequest, connectionId, requestId);
		this.imageId = imageId;
		this.serviceType = serviceType;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, "png", baos);
			baos.flush();
			this.image = baos.toByteArray();
			baos.reset();
			ImageIO.write(reference, "png", baos);
			baos.flush();
			this.reference = baos.toByteArray();
			baos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public synchronized BufferedImage getReference() {
		InputStream in = new ByteArrayInputStream(this.reference);
		try {
			BufferedImage bImageFromConvert = ImageIO.read(in);
			return bImageFromConvert;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	public synchronized BufferedImage getImage() {
		InputStream in = new ByteArrayInputStream(this.image);
		try {
			BufferedImage bImageFromConvert = ImageIO.read(in);
			return bImageFromConvert;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getImageId() {
		return this.imageId;
	}

	public String getServiceType() {
		return this.serviceType;
	}

	@Override
	public String toString() {
		return super.toString() + " " + imageId + " " + serviceType;
	}

}
