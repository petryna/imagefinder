package imagefinder.core;

import java.awt.Rectangle;
import java.util.LinkedList;
import java.util.List;

/**
 * Class that represents a generic ImageFinder. Performs the desired search
 * operation on the Image.
 *
 */
public abstract class AbstractFinder implements Runnable {

	protected FinderRequest request;
	protected List<Rectangle> matches;

	private FinderResult answer;
	private boolean started;

	public AbstractFinder(FinderRequest request) {
		this.request = request;
		this.matches = new LinkedList<Rectangle>();
		this.answer = null;
		this.started = false;
	}

	protected abstract void search();

	@Override
	public synchronized void run() {
		this.started = true;
		search();
		this.answer = new FinderResult(this.matches, this.request);
		notifyAll();
	}

	public synchronized FinderResult getResult() throws InterruptedException {
		
		if(!this.started) {
			throw new IllegalStateException("The search was not performed yet!");
		}
		
		while(this.answer == null) {
			wait();
		}
		
		return this.answer;
	}

	@Override
	public abstract String toString();

}
