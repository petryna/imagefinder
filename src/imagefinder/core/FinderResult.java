package imagefinder.core;

import java.awt.Rectangle;
import java.util.List;

public class FinderResult extends Message implements Comparable<FinderResult> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5558048316008643177L;

	private String serviceType;

	/**
	 * List of Rectangle objects representing detected matches on the image.
	 */
	private List<Rectangle> matches;

	/**
	 * Identifier for the image.
	 */
	private String imageId;

	private String representation;

	public FinderResult(List<Rectangle> matches, FinderRequest request) {
		super(Message.MessageType.FinderResult, request.getConnectionId(), request.getRequestId());
		this.matches = matches;
		this.imageId = request.getImageId();
		this.serviceType = request.getServiceType();
		this.representation = this.imageId + " - " + this.matches.size() + " " + serviceType;
	}

	public List<Rectangle> getMatches() {
		return this.matches;
	}

	public String getImageId() {
		return this.imageId;
	}

	public String getServiceType() {
		return serviceType;
	}

	public String getRepresentatin() {
		return representation;
	}

	@Override
	public String toString() {
		return super.toString() + " " + representation;
	}

	@Override
	public int compareTo(FinderResult other) {
		return this.toString().compareTo(other.toString());
	}

}
