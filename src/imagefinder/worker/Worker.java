package imagefinder.worker;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import imagefinder.core.AbstractFinder;
import imagefinder.core.FinderRequest;
import imagefinder.core.FinderResult;
import imagefinder.core.RotationalFinder;
import imagefinder.core.SimpleFinder;
import imagefinder.core.WorkerConnectionRequest;
import imagefinder.pcd.networking.Connection;
import imagefinder.pcd.networking.SocketConnection;

public class Worker extends Thread {

	private Connection connection;
	@SuppressWarnings("unused")
	private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
	private PropertyChangeListener connectionListener;
	private String hostname;
	private String serviceType;
	private static final int PORT = 3080;
	private static Map<String, RotationalFinder.Rotation> servicesLibrary = createMap();

	public Worker(String hostname, String workerType) {
		this.hostname = hostname;
		this.serviceType = workerType;
		connectToServer();
		start();
	}

	private static Map<String, RotationalFinder.Rotation> createMap() {
		Map<String, RotationalFinder.Rotation> map = new HashMap<String, RotationalFinder.Rotation>();
		map.put("90Search", RotationalFinder.Rotation.R90);
		map.put("180Search", RotationalFinder.Rotation.R180);
		map.put("270Search", RotationalFinder.Rotation.R270);
		return map;
	}

	private void connectToServer() {
		try {
			connection = new SocketConnection(hostname, PORT);
		} catch (IOException e) {
			System.err.println("No connection to server!");
			System.exit(1);
		}
		connectionListener = new ListenerManager();
		connection.addConnectionListener(connectionListener);
		connection.sendObject(new WorkerConnectionRequest(serviceType));
	}

	private class ListenerManager implements PropertyChangeListener {
		public void propertyChange(PropertyChangeEvent e) {
			System.out.println("Connection lost");
			connectToServer();
		}
	}

	@Override
	public void run() {
		while (!interrupted()) {
			Object request;
			try {
				request = connection.receiveObject();
				if (request != null && request instanceof FinderRequest) {
					AbstractFinder finder = null;
					switch (serviceType) {
					case "SimpleSearch":
						finder = new SimpleFinder((FinderRequest) request);
						break;
					default:
						if (servicesLibrary.containsKey(serviceType)) {
							finder = new RotationalFinder((FinderRequest) request, servicesLibrary.get(serviceType));
						} else {
							connection.sendObject(request);
							return;
						}
					}
					System.out.println(request);
					finder.run();
					FinderResult result = finder.getResult();
					System.out.println(result);
					connection.sendObject(result);
				}
			} catch (InterruptedException e) {
				connectToServer();
			}
		}
	}

	public static void main(String[] args) {
		if (args.length > 1) {
			new Worker(args[0], args[1]);
		} else {
			System.err.println("Hostname or FinderType missing!");
		}
	}

}
